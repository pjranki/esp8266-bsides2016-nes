BSides Entertainment System (2016)
==================================
NES emulator running on the 2016 BSides Canberra Badge.

Required Libraries
------------------
crc32fast
nes
SPI
Adafruit_ST7735

Games
-----
You need to use your own games, the nes library has a script to convert them to C headers.
